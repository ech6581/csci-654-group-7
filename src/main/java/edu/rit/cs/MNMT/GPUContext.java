package edu.rit.cs.MNMT;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;

import java.io.InputStream;
import java.math.BigInteger;
import java.util.Arrays;

import static jcuda.driver.JCudaDriver.*;
import static jcuda.runtime.JCuda.*;
import static jcuda.runtime.JCuda.cudaGetErrorString;
import static jcuda.runtime.cudaError.cudaSuccess;
import static jcuda.runtime.cudaFuncCache.cudaFuncCachePreferShared;

/**
 * GPUContext - Attempt to segregate all of the "static" GPU Device information
 * to allow for "context-switching" from our Java threads.
 */
public class GPUContext {
    public int deviceId;
    public CUdevice device;
    public CUcontext context;
    public CUmodule module;
    public CUfunction function;
    public CUdeviceptr d_targetHashBytes;
    public CUdeviceptr d_in;
    public CUdeviceptr device_out_nonce;
    public CUdeviceptr g_out;
    public CUdeviceptr g_hash_out;
    public CUdeviceptr g_found;
    public CUdeviceptr dev_k;
    public CUdeviceptr dev_HEXBYTES;
    public byte in[];
    public long nonce;

    // Constructor
    public GPUContext(int deviceId) {
        this.deviceId = deviceId;

        // Enable exceptions and omit all subsequent error checks
        JCudaDriver.setExceptionsEnabled(true);

        // Initialize the driver and create a context for the first device.
        device = new CUdevice();
        cuDeviceGet(device, this.deviceId);             // Get's the 0th device
        context = new CUcontext();
        cudaSetDevice(this.deviceId);
        cuCtxCreate(context, 0, device);
        cudaCheckErrors("deviceSetup");

        // Set the cache configuration
        cuCtxSetCacheConfig(cudaFuncCachePreferShared);
        cudaCheckErrors("cacheConfig");

        // Extrat the SHA256.ptx as a resource into a byte array
        // SHA256 code based upon: https://github.com/moffa13/SHA256CUDA/blob/master/SHA256CUDA/main.cpp
        InputStream inputStream = getClass().getResourceAsStream("/SHA256.ptx");
        byte ptxData[] = Utilities.toZeroTerminatedStringByteArray(inputStream);

        // Load the module data and ptx data
        module = new CUmodule();
        cuModuleLoadData(module, ptxData);
        cudaCheckErrors("loadModule");

        // Obtain a function pointer to the "sha256_kernel" function.
        function = new CUfunction();
        //cuModuleGetFunction(function, module, "sha256_kernel_vertical");
        cuModuleGetFunction(function, module, "sha256_kernel");
        cudaCheckErrors("getKernel");
    }

    public void initMemory(String targetHash, String blockHash, int startNonce) {
        cuCtxSetCurrent(context);

        // Let's get the targetHashBytes in the form of a byte array
        // Perform some magic here so we don't lose the leading 0s!!!
        //byte[] targetHashBytesPrepended1 = new BigInteger("1" + targetHash, 16).toByteArray();
        byte[] targetHashBytesPrepended1 = new BigInteger("10" + targetHash, 16).toByteArray();   // Prepend hex value of "10" so no leading zeros.
        byte[] targetHashBytes = Arrays.copyOfRange(targetHashBytesPrepended1, 1, targetHashBytesPrepended1.length);  // Skip 1st byte (prepended hex "10") to get raw bytes array.
        d_targetHashBytes = new CUdeviceptr();
        cuMemAlloc(d_targetHashBytes, targetHashBytes.length * Sizeof.BYTE);
        cuMemcpyHtoD(d_targetHashBytes, Pointer.to(targetHashBytes), targetHashBytes.length * Sizeof.BYTE);
        cudaCheckErrors("targetHashBytesSetup");

        // Get the raw byte data for our blockHash
        in = Utilities.stringToCString(blockHash);

        // Create the input string for the device
        d_in = new CUdeviceptr();
        cuMemAlloc(d_in, in.length*Sizeof.BYTE);

        // Initialize d_in with in[] contents
        cuMemcpyHtoD(d_in, Pointer.to(in), in.length * Sizeof.BYTE);
        cudaCheckErrors("memcpy(d_in)");

        // Setup our output values for the kernel
        device_out_nonce = new CUdeviceptr();
        cuMemAlloc(device_out_nonce, 1 * Sizeof.INT);;
        g_out = new CUdeviceptr();
        cuMemAlloc(g_out, in.length + 32);
        g_hash_out = new CUdeviceptr();
        cuMemAlloc(g_hash_out, 32);
        g_found = new CUdeviceptr();
        cuMemAlloc(g_found, Sizeof.INT);

        // Initialize g_found with 0 value.
        int h_g_found[] = new int[1];
        h_g_found[0] = 0;
        cuMemcpyHtoD(g_found, Pointer.to(h_g_found), Sizeof.INT);
        cudaCheckErrors("memcpy(g_found)");

        // Setup the nonce
        nonce = startNonce;

        ///
        // Setup the SHA256 dev_k constants
        ///
        // Obtain the pointer to the constant memory, and print some info
        dev_k = new CUdeviceptr();
        long dev_kSizeArray[] = { 0 };
        cuModuleGetGlobal(dev_k, dev_kSizeArray,
                module, "dev_k");
        int dev_kMemorySize = (int)dev_kSizeArray[0];

        // Copy some host data to the constant memory
        cuMemcpyHtoD(dev_k,
                Pointer.to(GPUNonceThread.host_k), dev_kMemorySize);

        ///
        // Setup the SHA256 dev_HEXBYTES constants
        ///
        // Obtain the pointer to the constant memory, and print some info
        dev_HEXBYTES = new CUdeviceptr();
        long dev_HEXBYTESSizeArray[] = { 0 };
        cuModuleGetGlobal(dev_HEXBYTES, dev_HEXBYTESSizeArray,
                module, "dev_HEXBYTES");
        int dev_HEXBYTESMemorySize = (int)dev_HEXBYTESSizeArray[0];

        // Copy some host data to the constant memory
        cuMemcpyHtoD(dev_HEXBYTES,
                Pointer.to(GPUNonceThread.host_HEXBYTES), dev_HEXBYTESMemorySize);

        cudaCheckErrors("pre_SHA256");
    }

    // Helper method to free memory
    public void freeMemory() {
        cuCtxSetCurrent(context);

        // Clean up.
        cuMemFree(g_out);
        cuMemFree(g_hash_out);
        cuMemFree(g_found);
        cuMemFree(d_in);
        cuMemFree(d_targetHashBytes);
        cuMemFree(device_out_nonce);

        // Reset the device for next user...
        cudaDeviceReset();
    }

    /**
     * Checks if any CUDA errors have occurred and prints out the details if so.
     */
    void cudaCheckErrors(String info) {
        int lastError = cudaGetLastError();
        if(lastError != cudaSuccess) {
            System.out.println("CUDA ERROR: " + info + ", name=" + cudaGetErrorName(lastError) + ", string=" + cudaGetErrorString(lastError));
        } else {
            //System.out.println("CUDA SUCCESS: " + info);
        }
    }

}
