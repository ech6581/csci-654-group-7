package edu.rit.cs.MNMT;

import jcuda.runtime.cudaDeviceProp;

import java.io.*;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static jcuda.driver.JCudaDriver.cuInit;
import static jcuda.runtime.JCuda.cudaGetDeviceCount;
import static jcuda.runtime.JCuda.cudaGetDeviceProperties;
import static jcuda.runtime.cudaError.cudaSuccess;

/**
 * NonceSlave - Represents the "Slave" nodes in our Master-Slave architecture...
 */
public class NonceSlave  implements NonceThreadListener {
    public int port;
    public String masterAddress;
    public Socket socket;
    private boolean winningNonceFound = false;
    private String winningHash = null;
    private int winningNonce;
    public boolean processingComplete = false;
    public static Object lock = new Object();
    public int rank;
    public boolean processingBlock = true;
    public boolean processingBlocks = true;

    public long transitTime;
    public long hashRatePerMillisecond;
    public long prefetchThreshold = 0;
    private final int prefetchThresholdBuffer = 30000; //10000;

    private int nonceCounter = 0;
    private int hashAlgorithm;
    private List<Thread> threadList;
    private List<NonceThread> nonceThreadList;

    public CountDownLatch latch;

    //private List<NonceManagerMessage> nonceRangeList = new ArrayList<NonceManagerMessage>();
    private NonceQueue nonceQueue = new NonceQueue();
    private NonceManagerMessage configurationMessage;

    public NonceSlaveCommunicator communicator;
    public boolean gpuEnabled = false;
    public int gpuCount = 0;
    public int deviceId = 0;

    // GPU hash rate = 100M per second (desired chunk size = 100M)
    public static int GPU_HASH_RATE_PER_MILLISECOND = 400000; //50000;
    //200_000; //10_000; //100_000;
    public static int GPU_NONCES_PER_ITERATION;

    // Constructor
    public NonceSlave(String masterAddress, int port, int gpuBlockSize, int gpuNoncesPerIteration) {
        this.masterAddress = masterAddress;
        this.port = port;
        NonceSlave.GPU_HASH_RATE_PER_MILLISECOND = gpuBlockSize / 1000;
        this.hashAlgorithm = NonceThread.APACHE_COMMONS;
        NonceSlave.GPU_NONCES_PER_ITERATION = gpuNoncesPerIteration;

        try {
            while(socket == null) {
                // Establish a connection to the NonceManager
                try {
                    socket = new Socket(masterAddress, port);
                    //System.out.println("Connected!");
                } catch (ConnectException e) {
                    //System.out.println("waiting to connect...");
                    try {
                        Thread.sleep(250, 0);
                    } catch (InterruptedException ee) {
                        // do nothing.
                    }
                }
            }

            // Let's create a dedicated communicator thread for handling communications with Nonce Manager...
            communicator = new NonceSlaveCommunicator(socket, this);
            Thread communicatorThread = new Thread(communicator);
            communicatorThread.start();

            // Loop until done processing blocks
            while(processingBlocks) {
                processingBlock = true;
                // Perform initial request for the first nonce range to process for this block...
                //System.out.println("requestNonceRange3");
                requestNonceRange();

                while (processingBlock) {
                    // Block until we receive a nonce range...
                    //System.out.println("Calling getNextNonceRange()");
                    NonceRange range = nonceQueue.getNextNonceRange();
                    //System.out.println("Completed getNextNonceRange()");

                    if(processingBlock && range == null) {
                        System.out.println("NonceSlave range is null, but still processingBlock, continue.");
                        continue;
                    }

                    // Bail out of processing loop if we need to STOP...
                    if (!processingBlock) {
                        break;
                    }

                    // Retrieve our rank
                    rank = configurationMessage.destination;
                    //System.out.println("Rank=" + rank);

                    // Lazily create our worker threads...
                    if (nonceThreadList == null) {
                        // Create a list of threads for stopping when winner found
                        threadList = new ArrayList<Thread>();
                        // Create a list of NonceThread objects for communication to stop when winner found
                        nonceThreadList = new ArrayList<NonceThread>();

                        // Detect any GPU capabilities for this slave node...
                        int[] devices = new int[1];
                        if(cudaSuccess == cudaGetDeviceCount(devices)) {
                            // Initialize the CUDA API in serial code.
                            cuInit(0);

                            // Mark this slave as GPU Enabled
                            this.gpuEnabled = true;
                            this.gpuCount = 0;

                            // Get the local machine's hostname
                            InetAddress myHost = InetAddress.getLocalHost();
                            String hostname = myHost.getHostName();

                            // Shuffle the deviceIds
                            List<Integer> shuffleList = new ArrayList<Integer>();
                            for(int i = 0; i < devices[0]; i++) {
                                shuffleList.add(i);
                            }
                            Collections.shuffle(shuffleList);

                            // Find a device to checkout...
                            for (int t = 0; t < devices[0]; t++) {
                                // Extract the shuffled Device ID
                                int shuffleDeviceId = shuffleList.get(t);
                                // Try to checkout this device from the master...
                                if (communicator.checkoutDevice(hostname, shuffleDeviceId)) {
                                    System.out.println("\rCheckout successful: deviceId=" + shuffleDeviceId + "                                ");

                                    // Set our gpuCount and deviceId for this slave
                                    this.gpuCount = 1;
                                    this.deviceId = shuffleDeviceId;
                                    break;
                                } else {
                                    //System.out.println("Checkout failed: deviceId=" + shuffleDeviceId);
                                }
                            }
                        }

                        // Setup workers depending on if we're GPU-based or not...
                        if(this.gpuCount == 1) {
                            // Create the NonceThread runnable
                            cudaDeviceProp prop = new cudaDeviceProp();
                            cudaGetDeviceProperties(prop, deviceId);
                            //System.out.println("device[" + deviceId + "] supports threads=" + prop.maxThreadsDim[0]);
                            //System.out.println("device[" + deviceId + "] supports threadsPerBlock=" + prop.maxThreadsPerBlock);
                            NonceThread nonceThread = new GPUNonceThread(this, hashRatePerMillisecond, transitTime, hashAlgorithm, deviceId, prop.maxThreadsPerBlock);

                            // Add the NonceThread to our nonceThread list
                            nonceThreadList.add(nonceThread);

                            // Create a new thread worker based upon NonceThread runnable
                            Thread worker = new Thread(nonceThread);

                            // Add the worker to our thread list
                            threadList.add(worker);

                            // Start the threads
                            worker.start();
                        } else {
                            //System.out.print("No GPU capability on this slave");

                            // Mark this Slave as NOT GPU Enabled
                            this.gpuEnabled = false;

                            // Kickoff a thread for each nonce chunk
                            for (int t = 0; t < configurationMessage.threads; t++) {

                                // Create the NonceThread runnable
                                NonceThread nonceThread = new NonceThread(this, hashRatePerMillisecond, transitTime, hashAlgorithm);

                                // Add the NonceThread to our nonceThread list
                                nonceThreadList.add(nonceThread);

                                // Create a new thread worker based upon NonceThread runnable
                                Thread worker = new Thread(nonceThread);

                                // Add the worker to our thread list
                                threadList.add(worker);

                                // Start the threads
                                worker.start();
                            }
                        }
                    }

                    // Perform proof-of-work, this is a blocking operation...
                    long startOfHashing = System.currentTimeMillis();
                    //System.out.print("Start of hashing!");
                    if (pow(range, threadList, nonceThreadList)) {
                        // Calculate our current hashing rate...
                        long hashDuration = System.currentTimeMillis() - startOfHashing;
                        hashRatePerMillisecond = (range.endNonce - range.startNonce) / hashDuration;

                        // We found a winning nonce!
                        NonceManagerMessage msg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_WINNER, rank, 0, this.winningNonce, this.winningHash, range.msg.blockHash);
                        communicator.sendMessage(msg);

                        // Stop processing the current block...
                        processingBlock = false;
                    } else {
                        // Calculate our current hashing rate...
                        long hashDuration = System.currentTimeMillis() - startOfHashing;
                        if(hashDuration != 0) {
                            hashRatePerMillisecond = (range.endNonce - range.startNonce) / hashDuration;
                        }
                    }

                    // Re-calculate our prefetch threshold...
                    if(this.gpuEnabled) {
                        this.prefetchThreshold = 10000000 * this.gpuCount;
                    } else {
                        this.prefetchThreshold = this.transitTime * 2 * this.hashRatePerMillisecond + this.prefetchThresholdBuffer;
                    }
                }
            }

            // Close down the connection
            socket.close();
        } catch (IOException e) {
            System.out.println("ERROR: IOException.");
            e.printStackTrace();
        }
    }

    /**
     * perform the proof-of-work
     * @return true if a winning nonce could be found, false otherwise.
     */
    private boolean pow(NonceRange range, List<Thread> threadList, List<NonceThread> nonceThreadList) {
        int startNonce = range.startNonce;
        int endNonce = range.endNonce;
        NonceManagerMessage msg = range.msg;

        // Initialize nonce counter
        resetNonceCount(endNonce - startNonce);

        // Initialize processingComplete
        processingComplete = false;

        // Reset winningNonce details
        winningNonceFound = false;

        // Let's create a noncethread to do our work...

        // Let's reset our countdownlatch
        latch = new CountDownLatch(threadList.size());

        // Calculate the chunk size
        int chunkSize = (endNonce - startNonce) / threadList.size();

        // Kickoff a thread for each nonce chunk
        for(int t = 0; t < threadList.size(); t++) {
            // Get the nonceThread
            NonceThread nonceThread = nonceThreadList.get(t);

            // Wait until the thread is in a state where it is ready for new work
            synchronized (nonceThread.readyLock) {
                //System.out.println("INFO: notifying on readyLock");
                nonceThread.readyLock.notify();
            }

            // Calculate thread nonce range...
            int threadStartNonce = startNonce + chunkSize*t;
            int threadEndNonce = threadStartNonce + chunkSize;
            if(threadEndNonce > endNonce)
                threadEndNonce = endNonce;

            // Assign the new nonce range to the thread
            nonceThread.setNonceRange(threadStartNonce, threadEndNonce, msg);

            // Notify thread that it can begin its round of work
            synchronized (nonceThread.lock) {
                //System.out.println("INFO: notifying on lock");
                nonceThread.lock.notify();
            }
        }

        // Wait until threads are done processing...
        try {
            //System.out.println("waiting on countdownlatch...");
            latch.await();
        } catch (InterruptedException e) {
            System.out.println("ERROR: interrupted waiting for latch countdown.");
            e.printStackTrace();
        }

        // When prefetch feature disabled, request next nonce range here...
        if(!winningNonceFound && this.hashAlgorithm < 0) {
            //System.out.println("requestNonceRange2");
            requestNonceRange();
        }

        return winningNonceFound;
    }

    @Override
    // Helper method to update processing state when winning nonce has been found...
    public void notifyWinningNonce(int winningNonce, String winningHash) {
        // Only if we haven't already found a winning nonce...
        if(!this.winningNonceFound) {
            //System.out.println("About to Notify All on lock");
            synchronized (NonceSlave.lock) {
                this.processingComplete = true;
                this.winningNonce = winningNonce;
                this.winningHash = winningHash;
                this.winningNonceFound = true;

                // Notify anyone waiting on the lock
                //System.out.println("Notifying All on lock");
                NonceSlave.lock.notifyAll();
            }
        }
    }

    @Override
    // Helper method to update processing state when winning nonce has been found...
    public void notifyNoWinningNonce() {
        synchronized (NonceSlave.lock) {
            this.processingComplete = true;

            // Notify anyone waiting on the lock
           // System.out.println("Notifying All on lock (no winner found)");
            NonceSlave.lock.notifyAll();
        }
    }

    // Helper method to stop a round of work for the current block
    public void stop() {
        //System.exit(0);

        //System.out.println("NonceSlave.stop()");
        processingBlock = false;

        // Cancel NonceThread getNextNonceRange()...
        for (NonceThread nonceThread : nonceThreadList) {
            nonceThread.nonceQueue.cancelGetNextNonceRange();
        }
        nonceQueue.cancelGetNextNonceRange();
    }

    // Helper method to terminate work
    public void terminate() {
        processingBlock = false;
        processingBlocks = false;
        nonceQueue.cancelGetNextNonceRange();
    }

    // Helper method to take in a new Nonce range
    public void newNonceRangeReceived(NonceManagerMessage msg) {
        // Calculate the transitTime from Master to Slave
        transitTime = System.currentTimeMillis() - msg.startAssignmentTime;
        this.configurationMessage = msg;
        nonceQueue.addNonceRange(msg.startNonce, msg.endNonce, msg);
    }

    // Helper method to reset the nonce countdown for prefetching
    private void resetNonceCount(int nonceCount) {
        nonceCounter = nonceCount;
    }

    // Helper method to request a new Nonce Range from the Master node
    private void requestNonceRange() {
        if(processingBlocks) {
            NonceManagerMessage msg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_REQUEST, rank, 0,
                    (gpuEnabled ? GPU_HASH_RATE_PER_MILLISECOND*gpuCount : hashRatePerMillisecond));
            NonceSlave.this.communicator.sendMessage(msg);
        }
    }

    // Decrements nonce prefetch counter and determines if prefetching needs to occur...
    public synchronized void considerNoncePrefetch(int decrement, long prefetchThreshold) {
        /*if(nonceCounter % 10000 == 0) {
            long threadId = Thread.currentThread().getId();
            System.out.println("NonceThread[" + threadId + "]: nonceCounter=" + nonceCounter + ", prefetchThreshold=" + prefetchThreshold);
        }*/

        nonceCounter -= decrement;
        if(nonceCounter < 0) nonceCounter = 0;

        // Determine if we need to request another nonce range...
        if(nonceCounter <= prefetchThreshold) {
            // Reset nonce count to MAX_VALUE, so we don't request another range until this range is exhausted
            resetNonceCount(Integer.MAX_VALUE);

            // Request another range
            requestNonceRange();
        }
    }
}
