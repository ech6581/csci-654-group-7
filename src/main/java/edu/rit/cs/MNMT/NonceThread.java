package edu.rit.cs.MNMT;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * NonceThread - class to perform proof-of-work computations
 *   Also the baseclass for GPUNonceThread
 */
public class NonceThread implements Runnable {
    private NonceSlave listener;
    private long prefetchThreshold = 0;
    public long hashRatePerMillis;
    public long transitTime;
    private final int prefetchThresholdBuffer = 30000; //10000;
    private int hashAlgorithm;
    public final Object lock = new Object();
    public final Object readyLock = new Object();
    public NonceQueue nonceQueue = new NonceQueue();

    // Available non-GPU SHA256 algorithms
    public final static int JAVA_BUILTIN = 1;
    public final static int APACHE_COMMONS = 2;

    // Constructor
    public NonceThread(NonceSlave listener, long hashRatePerMillis, long transitTime, int hashAlgorithm) {
        this.listener = listener;
        this.hashRatePerMillis = hashRatePerMillis;
        this.transitTime = transitTime;
        this.prefetchThreshold = transitTime*2 * hashRatePerMillis + prefetchThresholdBuffer;
        this.hashAlgorithm = hashAlgorithm;

        // Negative hashAlgorithm means disable pre-fetch feature
        if(hashAlgorithm < 0) {
            this.prefetchThreshold = 0;
            this.hashAlgorithm = this.hashAlgorithm * -1;
        }
    }

    // Sets the nonce range for this thread
    public void setNonceRange(int startNonce, int endNonce, NonceManagerMessage msg) {
        nonceQueue.addNonceRange(startNonce, endNonce, msg);
    }

    // Finished computations
    public void done() {
        listener.latch.countDown();
    }

    @Override
    // Perform proof of work computations
    public void run() {
        long endTime = System.currentTimeMillis();

        while(true) {
            // Let's block until we receive a NonceRange...
            NonceRange range = nonceQueue.getNextNonceRange();

            if(range == null) {
                if(listener.processingBlocks || listener.processingBlock) {
                    continue;
                }
                System.out.println("NonceThread range = null, stopping thread.");
                break;
            }

            int nonce = 0;
            String tmp_hash = "undefined";
            for (nonce = range.startNonce; nonce <= range.endNonce; nonce++) {
                // Stop if we received STOP or TERMINATE notification
                if(listener.processingBlock == false || listener.processingBlocks == false) {
                    // Someone else found the winner for this round, let's end this round gracefully...
                    break;
                }

                // Periodically consider opportunity to prefetch
                if (prefetchThreshold > 0 && nonce % 1000 == 0) {
                    if (nonce % 10000 == 0) {
                        //System.out.println("NonceThread[" + threadId + "]: nonceCounter=" + nonce + ", currentTime=" + System.currentTimeMillis());
                    }
                    listener.considerNoncePrefetch(1000, prefetchThreshold);
                }

                tmp_hash = SHA256(SHA256(range.msg.blockHash + String.valueOf(nonce), hashAlgorithm), hashAlgorithm);

                if (range.msg.targetHash.compareTo(tmp_hash) > 0) {
                    // Notify listener about our winning nonce!
                    //System.out.println("NonceThread[" + threadId + "]: notifying winning nonce");
                    listener.notifyWinningNonce(nonce, tmp_hash);
                    break;
                }
            }

            //System.out.println("NonceThread[" + threadId + "] is done with proof-of-work.");
            // Mark us as done!
            done();

            endTime = System.currentTimeMillis();
        }
    }

    /**
     * Code taken from: https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
     * get a sha256 of the input string
     * @param inputString
     * @return resulting hash in hex string
     */
    public String SHA256(String inputString, int algorithm) {
        switch (algorithm) {
            case JAVA_BUILTIN :
                try {
                    MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
                    return bytesToHex(sha256.digest(inputString.getBytes(StandardCharsets.UTF_8)));
                } catch (NoSuchAlgorithmException ex) {
                    System.err.println(ex.toString());
                    return null;
                }
            case APACHE_COMMONS :
                return DigestUtils.sha256Hex(inputString);
        }

        return null;
    }

    /**
     * Code taken from: https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
     * convert byte[] to hex string
     * @param hash
     * @return hex string
     */
    private String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
