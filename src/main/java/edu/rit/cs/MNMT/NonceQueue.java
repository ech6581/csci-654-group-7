package edu.rit.cs.MNMT;

import java.util.ArrayList;
import java.util.List;

/**
 * NonceQueue - Represents a blocking queue for taking NonceRange objects...
 */
public class NonceQueue {
    private List<NonceRange> nonceRangeList = new ArrayList<NonceRange>();
    private final Object available = new Object();

    public NonceQueue() {
    }

    // Adds a NonceRange to the end of the queue
    public void addNonceRange(int startNonce, int endNonce, NonceManagerMessage msg) {

        NonceRange range = new NonceRange(startNonce, endNonce, msg);
        nonceRangeList.add(range);

        // Notify any thread that is waiting on availability of a nonce range
        synchronized (available) {
            available.notifyAll();
        }

    }


    // Blocks until a NonceRange is available...
    public NonceRange getNextNonceRange() {

        // While empty...
        while(nonceRangeList.size() == 0) {
            // Block until a nonce range is available...
            synchronized (available) {
                try {
                    available.wait();

                    if(nonceRangeList.size() >= 1) {
                        return nonceRangeList.remove(0);
                    } else {
                        return null;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // While not empty...
        return nonceRangeList.remove(0);

    }

    // Cancels the current blocking operation...
    public void cancelGetNextNonceRange() {
        synchronized (available) {
            available.notifyAll();
        }
    }
}
