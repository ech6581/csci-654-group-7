package edu.rit.cs.MNMT;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.CountDownLatch;

/**
 * NonceSlaveCommunicator - responsible for all communications of NonceSlave
 */
public class NonceSlaveCommunicator implements Runnable {
    private Socket socket;
    private NonceSlave slave;
    private SendQueue sendQueue;
    private CountDownLatch checkoutLatch;
    private NonceManagerMessage checkoutResponse;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;

    // Constructor
    public NonceSlaveCommunicator(Socket socket, NonceSlave slave) {
        this.socket = socket;
        this.slave = slave;
        this.sendQueue = new SendQueue();

        /// Let's cache the socket communicatino object serialization objects
        try {
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Kickoff our dedicated "Sender" thread for serializing outbound communications from this particular slave...
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                     while(true) {
                         // Wait for next msg to be sent...
                         NonceManagerMessage msg = NonceSlaveCommunicator.this.sendQueue.getNextMsg();

                         if(msg != null) {
                             // Send the msg!
                             try {
                                 objectOutputStream.reset();
                                 objectOutputStream.writeObject(msg);
                                 objectOutputStream.flush();
                             } catch (IOException e) {
                                 //e.printStackTrace();
                             }
                         }
                     }
            }
        });
        sender.start();
    }

    @Override
    public void run() {
        while(true) {
            // Block until we receive our initial NonceManagerMessage
            NonceManagerMessage message = null;

            try {
                message = (edu.rit.cs.MNMT.NonceManagerMessage) objectInputStream.readObject();
            } catch (StreamCorruptedException | SocketException e) {
                // We received a corrupted stream, let's reconnect...
                try {
                    System.out.println("NonceSlaveCommunicator is reconnecting to maser.");
                    socket = new Socket(this.slave.masterAddress, this.slave.port);
                    this.slave.socket = socket;
                } catch (IOException ex) {
                    System.out.println("NonceSlaveCommunicator reconnection failure.");
                }
            } catch (IOException | ClassNotFoundException e) {
                //System.out.println("IOException or ClassNotFoundException");
                try {
                    socket.close();
                } catch (IOException ex) {
                    return;
                }
            }

            // If no message was returned, we're done...
            if(message == null) {
                return;
            }

            switch (message.type) {
                case NonceManagerMessage.MSG_TYPE_INITIALIZE:
                    slave.newNonceRangeReceived(message);
                    //System.out.println("Received INIT msg.");
                    break;
                case NonceManagerMessage.MSG_TYPE_STOP:
                    //System.out.println("Received STOP notification!");
                    slave.stop();
                    break;
                case NonceManagerMessage.MSG_TYPE_TERMINATE:
                    //System.out.println("rank: " + slave.rank + " Received TERMINATE notification!");
                    slave.terminate();
                    return;
                case NonceManagerMessage.MSG_TYPE_CHECKOUT_RESPONSE:
                    // Save the response into our dedicated checkoutResponse
                    this.checkoutResponse = message;

                    // Decrement the latch to complete the blocking request/response technique
                    this.checkoutLatch.countDown();
                    break;
                default:
                    System.out.println("Received UNKNOWN msg.");
                    break;
            }
        }
    }

    // Helper method to add a msg to the outbound queue
    public void sendMessage(NonceManagerMessage msg) {
        this.sendQueue.addMsg(msg);
    }


    // Helper method to initiate GPU Device checkout
    boolean checkoutDevice(String hostname, int deviceId) {
        // Send our request
        checkoutLatch = new CountDownLatch(1);
        NonceManagerMessage requestMsg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_CHECKOUT_REQUEST, 0, 0, hostname, deviceId, false);
        sendMessage(requestMsg);

        // Wait for response
        try {
            checkoutLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Inspect the response...
        if(checkoutResponse.checkoutResponse == true) {
            // Successful checkout!
            return true;
        }

        // Failed checkout...
        return false;
    }
}
