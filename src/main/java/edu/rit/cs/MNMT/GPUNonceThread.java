package edu.rit.cs.MNMT;

// Lots of imports required by jcuda...
import static jcuda.driver.JCudaDriver.*;
import static jcuda.runtime.JCuda.*;
import static jcuda.runtime.cudaError.cudaSuccess;

import jcuda.Pointer;
import jcuda.Sizeof;
import org.apache.commons.codec.digest.DigestUtils;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * GPUNonceThread - Subclass of NonceThread that performs all calculations using CUDA kernel
 */
public class GPUNonceThread extends NonceThread implements Runnable {
    private NonceSlave listener;
    private int hashAlgorithm;
    public NonceQueue nonceQueue = new NonceQueue();

    public final static int JAVA_BUILTIN = 1;
    public final static int APACHE_COMMONS = 2;

    private int deviceId;
    private int maxThreadsOnDevice;
    private GPUContext gpuContext;

    // Initialization constants to be loaded into SHA256.cu's dev_k constant memory...
    public static final int[] host_k = {
        0x428a2f98,0x71374491,0xb5c0fbcf,0xe9b5dba5,0x3956c25b,0x59f111f1,0x923f82a4,0xab1c5ed5,
                0xd807aa98,0x12835b01,0x243185be,0x550c7dc3,0x72be5d74,0x80deb1fe,0x9bdc06a7,0xc19bf174,
                0xe49b69c1,0xefbe4786,0x0fc19dc6,0x240ca1cc,0x2de92c6f,0x4a7484aa,0x5cb0a9dc,0x76f988da,
                0x983e5152,0xa831c66d,0xb00327c8,0xbf597fc7,0xc6e00bf3,0xd5a79147,0x06ca6351,0x14292967,
                0x27b70a85,0x2e1b2138,0x4d2c6dfc,0x53380d13,0x650a7354,0x766a0abb,0x81c2c92e,0x92722c85,
                0xa2bfe8a1,0xa81a664b,0xc24b8b70,0xc76c51a3,0xd192e819,0xd6990624,0xf40e3585,0x106aa070,
                0x19a4c116,0x1e376c08,0x2748774c,0x34b0bcb5,0x391c0cb3,0x4ed8aa4a,0x5b9cca4f,0x682e6ff3,
                0x748f82ee,0x78a5636f,0x84c87814,0x8cc70208,0x90befffa,0xa4506ceb,0xbef9a3f7,0xc67178f2
    };

    // Initialization constants to be loaded into SHA256.cu's dev_HEXTBYTES constant memory...
    public static final byte[] host_HEXBYTES = { (byte) '0', (byte) '1', (byte) '2', (byte) '3',
            (byte) '4', (byte) '5', (byte) '6', (byte) '7', (byte) '8', (byte) '9', (byte) 'a',
            (byte) 'b', (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f' };


    // Constructor
    public GPUNonceThread(NonceSlave listener, long hashRatePerMillis, long transitTime, int hashAlgorithm, int deviceId, int maxThreadsOnDevice) {
        super(listener, hashRatePerMillis, transitTime, hashAlgorithm);
        this.deviceId = deviceId;
        this.listener = listener;
        this.hashAlgorithm = hashAlgorithm;

        // Negative hashAlgorithm means disable pre-fetch feature
        if (hashAlgorithm < 0) {
            this.hashAlgorithm = this.hashAlgorithm * -1;
        }

        // Initialize our GPU Context...
        gpuContext = new GPUContext(deviceId);

        // Store the maximum number of threads
        this.maxThreadsOnDevice = maxThreadsOnDevice;
    }

    // Sets the current nonce range
    public void setNonceRange(int startNonce, int endNonce, NonceManagerMessage msg) {
        nonceQueue.addNonceRange(startNonce, endNonce, msg);
    }

    // Completed computations
    public void done() {
        /*
        synchronized (listener.threadsCompleted) {
            listener.threadsCompletedCount++;
            listener.threadsCompleted.notify();
        }*/
        listener.latch.countDown();
        //System.out.println("decremented countdown latch to " + listener.latch.getCount());
    }

    @Override
    // Performs work loop...
    public void run() {
        long endTime = System.currentTimeMillis();

        while (true) {
            // Let's block until we receive a NonceRange...
            NonceRange range = nonceQueue.getNextNonceRange();

            // Use CUDA-powered Double SHA algorithm...
            cudaDoubleSHA(range);

            // Mark us as done!
            done();

            endTime = System.currentTimeMillis();
        }
    }

    /**
     * Checks if any CUDA errors have occurred and prints out the details if so.
     */
    void cudaCheckErrors(String info) {
        int lastError = cudaGetLastError();
        if(lastError != cudaSuccess) {
            System.out.println("CUDA ERROR: " + info + ", name=" + cudaGetErrorName(lastError) + ", string=" + cudaGetErrorString(lastError));
        } else {
            //System.out.println("CUDA SUCCESS: " + info);
        }
    }


    // Performs SHA256 computations for the range by leveraging CUDA kernel
    // Work is divided across multiple GPU Rounds...
    public void cudaDoubleSHA(NonceRange range) {
        // Initialize the memory for our gpuContext
        long s1 = System.currentTimeMillis();
        gpuContext.initMemory(range.msg.targetHash, range.msg.blockHash, range.startNonce);
        //System.out.println("CUDA deviceId=" + this.deviceId + ", initMemory time=" + (System.currentTimeMillis() - s1) + "ms");

        // Setup the kernel dimensional design
        int BLOCK_SIZE = maxThreadsOnDevice;
        int NONCES_PER_ITERATIONS = NonceSlave.GPU_NONCES_PER_ITERATION;
        int NUMBLOCKS = (NONCES_PER_ITERATIONS + BLOCK_SIZE - 1) / BLOCK_SIZE;

        while(true) {
            //System.out.println("Performing GPU nonce round");
            if (listener.processingBlock == false || listener.processingBlocks == false) {
                //System.out.println("INFO: Bailing out of processing loop deviceId=[" + this.deviceId + "]");
                // Someone else found the winner for this round, let's end this round gracefully...
                break;
            }

            // Set up the kernel parameters: A pointer to an array
            // of pointers which point to the actual values.
            // This must align with the "sha256_kernel" function in SHA256.cu
            Pointer kernelParameters = Pointer.to(
                    Pointer.to(gpuContext.device_out_nonce),
                    Pointer.to(gpuContext.g_out),
                    Pointer.to(gpuContext.g_hash_out),
                    Pointer.to(gpuContext.g_found),
                    Pointer.to(gpuContext.d_targetHashBytes),
                    Pointer.to(gpuContext.d_in),
                    Pointer.to(new int[]{gpuContext.in.length - 1}),
                    Pointer.to(new int[]{NONCES_PER_ITERATIONS /* difficulty */}),
                    Pointer.to(new long[]{gpuContext.nonce})
            );
            cudaCheckErrors("kernelParameters");

            // Determine the size of shared memory
            int blockHashLength = gpuContext.in.length;
            int intermediateLength = 0;
            int ctxLength = 152;
            int dynamic_shared_size = (int) (Math.ceil((blockHashLength + ctxLength + 1) / 8.f) * 8) + (intermediateLength) * BLOCK_SIZE;

            // Call the kernel function.
            int gridSizeX = NUMBLOCKS;
            int blockSizeX = BLOCK_SIZE;

            long start = System.currentTimeMillis();

            // Set the context
            cuCtxSetCurrent(gpuContext.context);

            // Launch the kernel
            cuLaunchKernel(gpuContext.function,
                    gridSizeX, 1, 1,      // Grid dimension
                    blockSizeX, 1, 1,      // Block dimension
                    dynamic_shared_size, null,               // Shared memory size and stream
                    kernelParameters, null // Kernel- and extra parameters
            );
            cudaCheckErrors("launchKernel");

            // Wait for device to synchronized
            cuCtxSetCurrent(gpuContext.context);
            cudaDeviceSynchronize();
            cudaCheckErrors("cudaDeviceSynchronize");

            // Perform nonce prefetch considerations...
            listener.considerNoncePrefetch(NONCES_PER_ITERATIONS, listener.prefetchThreshold);

            cuCtxSynchronize();
            cudaCheckErrors("post synchronize");

            // Allocate host_g_found memory and copy from the device g_found
            int host_g_found[] = new int[1];
            cuMemcpyDtoH(Pointer.to(host_g_found), gpuContext.g_found, Sizeof.INT);

            // Kernel execution is asynchronous untli a memcpy is called, therefore this is the
            // "accurate" kernel execution time.
            //System.out.println("CUDA deviceId=" + this.deviceId + ", round duration=" + (System.currentTimeMillis() - start) + "ms");

            // Allocate host_out_nonce memory and copy from the device_out_nonce
            int host_out_nonce[] = new int[1];
            cuMemcpyDtoH(Pointer.to(host_out_nonce), gpuContext.device_out_nonce, Sizeof.INT);

            // Check if we found a suitable hash result...
            if (host_g_found[0] != 0) {
                // Allocate host g_hash_out memory and copy from the device g_hash_out
                byte host_g_hash_out[] = new byte[32];
                cuMemcpyDtoH(Pointer.to(host_g_hash_out), gpuContext.g_hash_out,
                        32 * Sizeof.BYTE);
                String hashString = bytesToHex(host_g_hash_out);

                // Allocate host_g_out memory and copy from the device g_out
                byte host_g_out[] = new byte[32];
                cuMemcpyDtoH(Pointer.to(host_g_out), gpuContext.g_out, 32 * Sizeof.BYTE);
                String nonceString = new String(host_g_out);

                // Debug output to confirm SHA256 CUDA equivalency with JAVA SHA256 implementation...
                /*System.out.println("======================================================");
                System.out.println("CUDA Found Nonce: [" + host_out_nonce[0] + "]");
                System.out.println("CUDA String Nonce: [" + nonceString + "]");
                System.out.println("Concatenated: [" + host_out_nonce[0] + range.msg.blockHash + "]");
                System.out.println("Target Hash    : [" + range.msg.targetHash + "]");
                System.out.println("CUDA Found Hash: [" + hashString + "]");
                String gold = Utilities.SHA256("" + range.msg.blockHash + host_out_nonce[0]);
                String goldDouble = Utilities.SHA256(gold);
                System.out.println("GOLD SHA256    : [" + gold + "]");
                System.out.println("GOLD SHA256X2  : [" + goldDouble + "]");
                */

               // Notify listener about our winning nonce!
               //System.out.println("Found winner!");
               listener.notifyWinningNonce(host_out_nonce[0], hashString);
                break;
            }

            // Increment nonce to next chunk...
            gpuContext.nonce += NONCES_PER_ITERATIONS;

            // Check to see if nonce range has been exhausted
            if(gpuContext.nonce > range.endNonce) {
                //System.out.println("Exhausted nonce range!");
                break;
            }
        }

        // Free resources from our context...
        gpuContext.freeMemory();
    }


    /**
     * get a sha256 of the input string
     *
     * @param inputString
     * @return resulting hash in hex string
     */
    public String SHA256(String inputString, int algorithm) {
        switch (algorithm) {
            case JAVA_BUILTIN:
                try {
                    MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
                    return bytesToHex(sha256.digest(inputString.getBytes(StandardCharsets.UTF_8)));
                } catch (NoSuchAlgorithmException ex) {
                    System.err.println(ex.toString());
                    return null;
                }
            case APACHE_COMMONS:
                return DigestUtils.sha256Hex(inputString);
        }

        return null;
    }

    /**
     * convert byte[] to hex string
     *
     * @param hash
     * @return hex string
     */
    private String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}