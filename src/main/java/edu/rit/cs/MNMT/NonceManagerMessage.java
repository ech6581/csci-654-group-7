package edu.rit.cs.MNMT;

import java.io.Serializable;

/**
 * NonceManagerMessage - the only Application Level Protocol message transferred between Master and Slave(s)
 */
public class NonceManagerMessage implements Serializable {
    public final static int MSG_TYPE_INITIALIZE = 0;
    public final static int MSG_TYPE_WINNER = 1;
    public final static int MSG_TYPE_REQUEST = 2;
    public final static int MSG_TYPE_STOP = 3;
    public final static int MSG_TYPE_TERMINATE = 4;
    public final static int MSG_TYPE_CHECKOUT_REQUEST = 5;
    public final static int MSG_TYPE_CHECKOUT_RESPONSE = 6;

    public int type = 0;
    public int source = 0;
    public int destination = 0;
    public int startNonce = 0;
    public int endNonce = 0;
    public String blockHash = "empty";
    public String targetHash = "empty";
    public String winningHash = "empty";
    public int winningNonce = 0;
    public int threads = 0;
    public long startAssignmentTime = 0;
    public int serialNumber = 0;
    public long hashRatePerMillis = 0;

    // Support checkout requests and success/fail for GPUs
    public String checkoutHostname;
    public int checkoutDeviceId;
    boolean checkoutResponse;

    // Constructor
    public NonceManagerMessage(int type, int source, int destination, int winningNonce, String winningHash, String blockHash) {
        this.type  = type;
        this.winningNonce = winningNonce;
        this.winningHash = winningHash;
        this.source = source;
        this.destination = destination;
        this.blockHash = blockHash;
    }

    // Constructor
    public NonceManagerMessage(int type, int source, int destination, String checkoutHostname, int checkoutDeviceId, boolean checkoutResponse) {
        this.type = type;
        this.source = source;
        this.destination = destination;
        this.checkoutHostname = checkoutHostname;
        this.checkoutDeviceId = checkoutDeviceId;
        this.checkoutResponse = checkoutResponse;
    }

    // Constructor
    public NonceManagerMessage(int type, int source, int destination, int startNonce, int endNonce, String blockHash, String targetHash, int threads, long startAssignmentTime) {
        this.type = type;
        this.source = source;
        this.destination = destination;
        this.startNonce = startNonce;
        this.endNonce = endNonce;
        this.blockHash = blockHash;
        this.targetHash = targetHash;
        this.threads = threads;
        this.startAssignmentTime = startAssignmentTime;
    }

    // Constructor
    public NonceManagerMessage(int type, int source, int destination) {
        this.type = type;
        this.source = source;
        this.destination = destination;
    }

    // Constructor
    public NonceManagerMessage(int type, int source, int destination, long hashRatePerMillis) {
        this.type = type;
        this.source = source;
        this.destination = destination;
        this.hashRatePerMillis = hashRatePerMillis;
    }
}