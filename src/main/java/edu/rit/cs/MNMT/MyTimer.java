package edu.rit.cs.MNMT;

/**
 * Code taken from https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/MyTimer.java
 *
 * Ensures using the same timing approach as coin_mining example code.
 */

public class MyTimer {
    private String timerName;
    private long startTime;
    private long endTime;
    private long elapsedTime;

    public MyTimer(String timerName){
        this.timerName = timerName;
    }

    public void start_timer(){
        this.startTime = System.nanoTime();
    }

    public void stop_timer(){
        this.endTime = System.nanoTime();
    }

    public void print_elapsed_time(){
        this.elapsedTime = this.endTime - this.startTime;
        double elapsedTimeInSecond = (double) this.elapsedTime / 1_000_000_000;
        System.out.println("ElapsedTime (" + this.timerName + "): "+ elapsedTimeInSecond + " seconds");
    }

    public double get_elapsed_time_in_sec() {
        return (double) this.elapsedTime / 1_000_000_000;
    }

}
