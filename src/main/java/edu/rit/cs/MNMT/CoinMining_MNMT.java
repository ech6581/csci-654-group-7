package edu.rit.cs.MNMT;

import java.io.IOException;

/**
 * Non-OpenMPI Entrypoint
 */
public class CoinMining_MNMT {

    public CoinMining_MNMT() {
        // Do initialization here
    }

    public static void main(String[] args) {
        long startTimeMillis = System.currentTimeMillis();

        if(args.length != 4 && args.length != 5) {
            System.out.println("Incorrect number of arguments specified.");
            return;
        }

        // Determine if we are to be the Nonce Manager Node or the Slave node
        if(args[0].contains("master")) {
            System.out.println("Master Mode...");

            String blockHash = Utilities.SHA256("CSCI-654 Foundations of Parallel Computing");
            //String targetHash = "0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
            String targetHash = "0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
            //String targetHash = "0000000a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";

            // 1th argument is port number master will listen for connections on
            int port = Integer.parseInt(args[1]);

            // 2th argument is the number of threads per slave
            int threads = Integer.parseInt(args[2]);

            // 3th argument is the nonce chunk size sent to slaves
            int nonceChunkSize = Integer.parseInt(args[3]);

            // Start-up our Nonce Master
            try {
                NonceMaster master = new NonceMaster(port, blockHash, targetHash, threads, nonceChunkSize);
                System.out.println("Resulting Hash: " + master.manager.winningHash);
                System.out.println("Nonce:" + master.manager.winningNonce);
            } catch (IOException e) {
                System.out.println("NonceManager could not be created.");
                e.printStackTrace();
            }

        } else if(args[0].contains("slave")) {
            System.out.println("Slave Mode...");

            // 1th argument is master hostname/ip
            String masterAddress = args[1];

            // 2th argument is master listening port
            int port = Integer.parseInt(args[2]);

            // 3rd argument is GPU nonce chunk size
            int gpuNonceChunkSize = Integer.parseInt(args[3]);

            // 4th argument is GPU nonces per iteration
            int gpuNoncesPerIteration = Integer.parseInt(args[4]);

            // Start-up our Nonce Slave
            NonceSlave slave = new NonceSlave(masterAddress, port, gpuNonceChunkSize, gpuNoncesPerIteration);
        }

        System.out.println("Elapsed time = " + (System.currentTimeMillis() - startTimeMillis) / 1000.0f + " seconds.");
        System.exit(0);
    }
}
