package edu.rit.cs.MNMT;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * NonceMaster - Represents the "Master" node in the Master-Slave architecture
 */
public class NonceMaster {
    private boolean listening = true;
    private int rankCounter = 0;
    private ServerSocket serverSocket;
    List<Thread> threadList = new ArrayList<Thread>();

    public NonceManager manager;

    // Constructor
    public NonceMaster(int port, String blockHash, String targetHash, int threads, int nonceChunkSize) throws IOException {
        // Create our listener on the given port
        this.serverSocket = new ServerSocket(port);

        // Create the NonceManager which orchestrates all Master/Slave activities
        this.manager = new NonceManager(serverSocket, nonceChunkSize, blockHash, targetHash, NonceMaster.this, threads);

        // Begin work for the NonceManager in a dedicated thread, this is a blocking operation...
        Thread nonceManagerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // Hoakie synchronization of slave-to-master connections that need to be in place before we commence work...
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //System.out.println("manager.beginWork()");
                manager.beginWork();
            }
        });
        nonceManagerThread.start();

        // Loop until we need to stop listening for connections...
        while(listening) {
            try {
                // Wait until a connection is acepted
                Socket socket = serverSocket.accept();
                //System.out.println("Client connection accepted.");

                // Create a dedicated communications thread for this client...
                rankCounter++;
                NonceManagerCommunicator communicator = new NonceManagerCommunicator(socket, rankCounter, manager);
                manager.addNonceManagerCommunicator(communicator);
                Thread communicatorThread = new Thread(communicator);
                threadList.add(communicatorThread);

                // Start the communicator thread
                communicatorThread.start();
            } catch (IOException e) {
                if(listening) {
                    System.out.println("ERROR: IOException.");
                    e.printStackTrace();
                } else {
                    // Normal behavior, since socket is closed on purpose.
                }
            }
        }

        // Notify all slaves to stop
        //System.out.println("Notifying all slaves to STOP.");
        for(NonceManagerCommunicator communicator : manager.getNonceManagerCommunicatorList()) {
            if(communicator.isProcessingBlock()) {
                //System.out.println("Stopping processing for NonceManagerCommunicator-rank-" + communicator.rank);
                communicator.stopProcessing();
            }
        }

        // Now that we're done listening, let's shutdown gracefully...
        for(Thread thread: threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println("join interrupted.");
            }
        }

    }

    // Helper method to stop listening for additional Slave connections...
    public void stopListening() {
        // Stop listening
        listening = false;

        // Close down our server socket
        try {
            //System.out.println("stopListening serverSocket.close()");
            serverSocket.close();
        } catch (IOException e) {
            System.out.println("IOException while closing serverSocket.");
        }
    }

}
