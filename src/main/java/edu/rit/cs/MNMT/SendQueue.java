package edu.rit.cs.MNMT;

import java.util.ArrayList;
import java.util.List;

/**
 * Blocking Queue mechanism for temporary storage of outbound NonceManagerMessage(s)
 */
public class SendQueue {
    private List<NonceManagerMessage> msgList = new ArrayList<NonceManagerMessage>();
    private final Object available = new Object();

    public SendQueue() {
    }

    public void addMsg(NonceManagerMessage msg) {
        msgList.add(msg);

        // Notify any thread that is waiting on availability of a nonce range
        synchronized (available) {
            available.notifyAll();
        }

    }

    public NonceManagerMessage getNextMsg() {

        // While empty...
        while(msgList.size() == 0) {
            // Block until a msg is available...
            synchronized (available) {
                try {
                    available.wait();

                    if(msgList.size() >= 1) {
                        return msgList.remove(0);
                    } else {
                        return null;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // While not empty...
        return msgList.remove(0);

    }

    public void cancelGetNextMsg() {
        synchronized (available) {
            available.notifyAll();
        }
    }
}
