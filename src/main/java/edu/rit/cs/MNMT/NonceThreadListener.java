package edu.rit.cs.MNMT;

/**
 * Interface for receiving notifications from NonceThread(s)
 */
public interface NonceThreadListener {
    /**
     * Notifies the listener with details of the winning nonce
     *
     * @param winningNonce the nonce solution for the hashing
     * @param winningHash the winning hash for the nonce
     */
    public void notifyWinningNonce(int winningNonce, String winningHash);

    public void notifyNoWinningNonce();

    public void considerNoncePrefetch(int decrement, long prefetchThreshold);
}
