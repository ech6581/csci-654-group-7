package edu.rit.cs.MNMT;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utilities {

    // Code taken from: https://stackoverflow.com/questions/17737206/efficiently-convert-java-string-into-null-terminated-byte-representing-a-c-str
    public static byte[] stringToCString(String string) {
        try {
            byte[] stringBytes = string.getBytes("ISO-8859-1");
            byte[] ntBytes = new byte[stringBytes.length + 1];
            System.arraycopy(stringBytes, 0, ntBytes, 0, stringBytes.length);

            return ntBytes;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Taken from: https://stackoverflow.com/questions/34259441/jcuda-cumoduleload-cannot-load-file-using-the-path-of-getclass-getresource
     *
     * Read the contents of the given input stream, and return it
     * as a byte array containing the ZERO-TERMINATED string data
     * from the stream. The caller is responsible for closing the
     * given stream.
     *
     * @param inputStream The input stream
     * @return The ZERO-TERMINATED string byte array
     * @throws IOException If an IO error occurs
     */
    public static byte[] toZeroTerminatedStringByteArray(
            InputStream inputStream)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte buffer[] = new byte[8192];
        while (true)
        {
            int read = 0;
            try {
                read = inputStream.read(buffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (read == -1)
            {
                break;
            }
            baos.write(buffer, 0, read);
        }
        baos.write(0);
        return baos.toByteArray();
    }

    /**
     * convert byte[] to hex string
     * https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
     * @param hash
     * @return hex string
     */
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * get a sha256 of the input string
     * https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
     * @param inputString
     * @return resulting hash in hex string
     */
    public static String SHA256(String inputString) {
        try {
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            return bytesToHex(sha256.digest(inputString.getBytes(StandardCharsets.UTF_8)));
        }catch (NoSuchAlgorithmException ex) {
            System.err.println(ex.toString());
            return null;
        }
    }

    // Code taken from: https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
    public static String HexValueDivideBy(String hexValue, int val) {
        BigInteger tmp = new BigInteger(hexValue,16);
        tmp = tmp.divide(BigInteger.valueOf(val));
        String newHex = bytesToHex(tmp.toByteArray());
        while (newHex.length() < hexValue.length()) {
            newHex = '0' + newHex;
        }
        return newHex;
    }

    // Code taken from: https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
    public static String HexValueMultipleBy(String hexValue, int val) {
        BigInteger tmp = new BigInteger(hexValue,16);
        tmp = tmp.multiply(BigInteger.valueOf(val));
        String newHex = bytesToHex(tmp.toByteArray());
        while (newHex.length() < hexValue.length()) {
            newHex = '0' + newHex;
        }
        return newHex;
    }
}
