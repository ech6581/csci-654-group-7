package edu.rit.cs.MNMT;

/**
 * Encapsulates a Nonce range which includes start and end nonces...
 */
public class NonceRange {
    public int startNonce;
    public int endNonce;
    public NonceManagerMessage msg;

    public NonceRange(int startNonce, int endNonce) {
        this.startNonce = startNonce;
        this.endNonce = endNonce;
        this.msg = null;
    }

    public NonceRange(int startNonce, int endNonce, NonceManagerMessage msg) {
        this.startNonce = startNonce;
        this.endNonce = endNonce;
        this.msg = msg;
    }
}
