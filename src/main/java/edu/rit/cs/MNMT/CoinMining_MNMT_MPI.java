package edu.rit.cs.MNMT;

import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;

/**
 * OpenMPI entry point
 */
public class CoinMining_MNMT_MPI {

    public CoinMining_MNMT_MPI() {
        // Do initialization here
    }

    public static void main(String[] args) throws MPIException {
        long startTimeMillis = System.currentTimeMillis();

        // Initialization for MPI
        MPI.Init(args);

        // 1th argument is master hostname/ip
        String masterAddress = args[0];

        // 1th argument is port number master will listen for connections on
        int port = Integer.parseInt(args[1]);

        // 2th argument is the number of threads per slave
        int threads = Integer.parseInt(args[2]);

        // 3th argument is the nonce chunk size sent to slaves
        int nonceChunkSize = Integer.parseInt(args[3]);

        // 3rd argument is GPU nonce chunk size sent to slaves
        int gpuNonceChunkSize = Integer.parseInt(args[4]);

        // 6th argument is GPU noncesPerIteration sent to slaves
        int gpuNoncesPerIteration = Integer.parseInt(args[5]);

        int rank = MPI.COMM_WORLD.getRank();

        // Determine if we are to be the Nonce Manager Node or the Slave node
        if(rank == 0) {
            // Master mode...
            //System.out.println("Master Mode...");

            String blockHash = Utilities.SHA256("CSCI-654 Foundations of Parallel Computing");
            //String targetHash = "0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
            String targetHash = "0000092a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
            //String targetHash = "00999992a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
            //String targetHash = "0000002a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
            //String targetHash = "0000000a6893b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";

            // Start-up our Nonce Master
            try {
                NonceMaster master = new NonceMaster(port, blockHash, targetHash, threads, nonceChunkSize);
                //System.out.println("Resulting Hash: " + master.manager.winningHash);
                //System.out.println("Nonce:" + master.manager.winningNonce);
            } catch (IOException e) {
                System.out.println("NonceManager could not be created.");
                e.printStackTrace();
            }

        } else {
            //System.out.println("Slave Mode...");

            // Start-up our Nonce Slave
            NonceSlave slave = new NonceSlave(masterAddress, port, gpuNonceChunkSize, gpuNoncesPerIteration);
            //System.out.println("Slave rank " + MPI.COMM_WORLD.getRank() + " finished!");
        }

        // Finalization for MPI
        //MPI.COMM_WORLD.barrier();
        MPI.Finalize();

        if(rank == 0) {
            System.out.println("Elapsed time = " + (System.currentTimeMillis() - startTimeMillis) / 1000.0f + " seconds.");
        }
        System.exit(0);
    }
}
