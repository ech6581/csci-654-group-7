package edu.rit.cs.MNMT;

/**
 * Block - Encapsulates a Block in the block chain
 */
public class Block {
    public String blockHash;
    public String targetHash;
    public int winningNonce;
    public String winningHash;

    public Block(String blockHash, String targetHash, int winningNonce, String winningHash) {
        this.blockHash = blockHash;
        this.targetHash = targetHash;
        this.winningNonce = winningNonce;
        this.winningHash = winningHash;
    }
}
