package edu.rit.cs.MNMT;

import java.util.ArrayList;
import java.util.List;

/**
 * BlockQueue - Blocking queue mechanism for handling Blocks in the Block Chain
 */
public class BlockQueue {
    private List<Block> blockList = new ArrayList<Block>();
    private final Object available = new Object();

    public BlockQueue() {
    }

    // Adds a block to the queue
    public void addBlock(Block block) {
        blockList.add(block);

        // Notify any thread that is waiting on availability of a block
        synchronized (available) {
            available.notifyAll();
        }

    }

    // Blocks until the next block is available
    public Block getNextBlock() {

        // While empty...
        while(blockList.size() == 0) {
            // Block until a block is available...
            synchronized (available) {
                try {
                    available.wait();
                    return blockList.remove(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        // While not empty...
        return blockList.remove(0);

    }

}
