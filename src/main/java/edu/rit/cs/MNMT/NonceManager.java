package edu.rit.cs.MNMT;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * NonceManager - Major functionality implementation of the "Master" Node.  Coordinates
 * the distribution of work to Slaves and tracks progress on blocks.
 */
public class NonceManager {
    private final int startNonce = Integer.MIN_VALUE;
    private final int endNonce = Integer.MAX_VALUE;
    private int currentNonce = startNonce;
    private int nonceChunkSize = 1000000; //10000000; //10; // 1000000;
    public int winningNonce = 0;
    public String winningHash = "";
    private ServerSocket serverSocket;
    private List<NonceManagerCommunicator> communicatorList = new ArrayList<NonceManagerCommunicator>();
    public BlockQueue blockQueue = new BlockQueue();

    private NonceMaster master;

    // Simple data structure to track hostname/device checkouts.
    private Map<String, List<Integer>> checkoutHostnameToDeviceIdList = new HashMap<String, List<Integer>>();

    // Values needed by NonceManagerCommunicator(s)
    public String blockHash;
    public String targetHash;
    public int threads;

    // Constructor
    public NonceManager(ServerSocket serverSocket, int nonceChunkSize, String initBlockHash, String initTargetHash, NonceMaster master, int threads) {
        this.serverSocket = serverSocket;
        this.nonceChunkSize = nonceChunkSize;

        // Needed by NonceManagerCommunicator(s)
        this.blockHash = initBlockHash;
        this.targetHash = initTargetHash;
        this.threads = threads;

        // Needed for setWinner to be callable and useful from NonceManagerCommmunicator(s)
        this.master = master;
    }

    // Blocking method...
    // Performs all block chain work, based on https://gitlab.com/SpiRITlab/parallelcomputing/blob/master/coin_mining/src/main/java/edu/rit/cs/CoinMining_Seq.java
    public void beginWork() {

        // number of blocks to be generated or number of rounds; default to 5
        int numberOfBlocks=10;

        // average block generation time, default to 30 Secs.
        double avgBlockGenerationTimeInSec = 30.0;

        int currentBlockID = 1;
        int nonce = 0;

        MyTimer myTimer;
        while(currentBlockID <= numberOfBlocks) {
            // Reset the nonce range back to initial value...
            resetNonceRange();

            System.out.println("Processing blockID=" + currentBlockID);
            myTimer = new MyTimer("CurrentBlockID:"+currentBlockID);
            myTimer.start_timer();

            // assign jobs to worker
            Block block = pow(blockHash, targetHash);

            myTimer.stop_timer();
            System.out.println("==============================");
            myTimer.print_elapsed_time();

            // found a new block
            blockHash = Utilities.SHA256(blockHash+"|"+nonce);

            // update the target
            if(myTimer.get_elapsed_time_in_sec()<avgBlockGenerationTimeInSec)
                targetHash = Utilities.HexValueDivideBy(targetHash, 2);
            else
                targetHash = Utilities.HexValueMultipleBy(targetHash, 2);

            System.out.println("Nonce: " + block.winningNonce);
            System.out.println("OTarget Hash: " + block.targetHash);
            System.out.println("Winning Hash: " + block.winningHash);

            System.out.println("New Block Hash:  " + blockHash);
            System.out.println("New Target Hash: " + targetHash);
            System.out.println();
            currentBlockID++;
        }

        // Terminate all slaves...
        for (NonceManagerCommunicator communicator : communicatorList) {
            communicator.terminateProcessing();
        }

        // Notify Master to stop
        master.stopListening();
    }

    // Coordinates proof-of-work with Slaves...
    private Block pow(String blockHash, String targetHash) {
        // Notify communicators with the new criteria so they can begin work...
        for (NonceManagerCommunicator communicator: communicatorList) {
            //System.out.println("Setting criteria for communicator...");
            communicator.setCriteria(blockHash, targetHash);
        }

        // Wait for winning block to be generated...
        //System.out.println("Waiting for winning block...");
        Block block = blockQueue.getNextBlock();

        // Return the winning nonce...
        //System.out.println("Received winning block!");
        return block;
    }

    // Helper method to track NonceManagerCommunicators...
    public void addNonceManagerCommunicator(NonceManagerCommunicator communicator) {
        communicatorList.add(communicator);
    }

    // Helper method to get NonceManagerCommunicators...
    public List<NonceManagerCommunicator> getNonceManagerCommunicatorList() {
        return communicatorList;
    }

    // Helper method to reset the current nonce to starting nonce...
    public void resetNonceRange() {
        currentNonce = startNonce;
    }

    // Helper method to assign the next nonce range
    public boolean assignNextNonceRange(NonceManagerMessage message, long hashRatePerMillis) {
        if(currentNonce == endNonce) {
            // Cannot assign any more nonce ranges, we're done!
            return false;
        }

        // Let's calculate the next available chunk
        int chunkStartNonce = currentNonce;

        // Let's scale the chunk according to the slave's hashing rate so that it
        // will take approximately 1 second worth of "work"
        int chunkEndNonce = currentNonce + (int) hashRatePerMillis * 1000;
        if((hashRatePerMillis * 1000) < nonceChunkSize) {
            // If hashRate is too slow, then assume default chunk size...
            chunkEndNonce = currentNonce + nonceChunkSize;
        }

        currentNonce = chunkEndNonce;

        if(chunkEndNonce > endNonce) {
            chunkEndNonce = endNonce;
            currentNonce = endNonce;
        }

        // Let's update the message with the next chunk...
        message.startNonce = chunkStartNonce;
        message.endNonce = chunkEndNonce;

        // Provide some progress output while calculations are occurring...
        System.out.print("\rcurrentNonce=" + currentNonce + ", hashRatePerMillis=" + hashRatePerMillis + "               ");

        return true;
    }

    // Helper method to set the winner
    public void setWinner(int winningNonce, String winningHash) {
        this.winningNonce = winningNonce;
        this.winningHash = winningHash;

        // Notify all slaves to stop their current round..
        for (NonceManagerCommunicator communicator: communicatorList) {
            communicator.stopProcecssingThisBlock();
        }

        // Add the winner to our blocking queue so that NonceMAnager's pow() can continue execution...
        this.blockQueue.addBlock(new Block(blockHash, targetHash, winningNonce, winningHash));
    }

    /**
     * helper method to manage device checkouts in a synchronous way...
     *
     * @param hostname the hostname requesting the checkout
     * @param deviceId the local device id requesting to be checked out
     * @return true if checkout successful, false otherwise;
     */
    public synchronized boolean checkoutDevice(String hostname, int deviceId) {
        // Lookup the hostname...
        List<Integer> deviceIdList = checkoutHostnameToDeviceIdList.get(hostname);
        if(deviceIdList == null) {
            // No device checked out yet for this hostname, lets be the first!
            deviceIdList = new ArrayList<Integer>();
            deviceIdList.add(deviceId);

            // Add the list to the map
            checkoutHostnameToDeviceIdList.put(hostname, deviceIdList);

            // Checkout successful!
            return true;
        } else {
            if (deviceIdList.contains(deviceId)) {
                // Fail, the device is already checked out on this host!
                return false;
            } else {
                // Success, the device is available!
                deviceIdList.add(deviceId);
                return true;
            }
        }
    }
}
