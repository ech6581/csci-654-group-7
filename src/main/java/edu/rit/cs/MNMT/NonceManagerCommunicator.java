package edu.rit.cs.MNMT;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.CountDownLatch;

/**
 * NonceManagerCommunicator - responsible for all "Master" (NonceMaster and NonceManager) communications
 */
public class NonceManagerCommunicator implements Runnable {
    private Socket socket;
    public int rank;
    private NonceManager manager;
    private String blockHash;
    private String targetHash;
    private int threads;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private boolean processingBlock = true;
    private boolean processingBlocks = true;

    private CountDownLatch criteria;
    private SendQueue sendQueue;

    // socket, rankCounter, manager);
    public NonceManagerCommunicator(Socket socket, int rank, NonceManager manager) {
        this.socket = socket;
        this.rank = rank;
        this.manager = manager;
        this.blockHash = manager.blockHash;
        this.targetHash = manager.targetHash;
        this.threads = manager.threads;

        /// Let's cache the socket communicatino object serialization objects
        try {
            objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectInputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Let's setup to wait until criteria has been set before beginning a round of work for the slave...
        this.criteria = new CountDownLatch(1);

        this.sendQueue = new SendQueue();

        // Kickoff our dedicated "Sender" thread for serializing outbound communications for this particular slave...
        Thread sender = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    // Wait for next msg to be sent...
                    NonceManagerMessage msg = NonceManagerCommunicator.this.sendQueue.getNextMsg();

                    if(msg != null) {
                        try {
                            objectOutputStream.reset();
                            objectOutputStream.writeObject(msg);
                            objectOutputStream.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        sender.start();
    }

    // Sets the new criteria for the current block...
    public void setCriteria(String blockHash, String targetHash) {
        // Setup the criteria for the next block to be mined...
        this.blockHash = blockHash;
        this.targetHash = targetHash;

        // Turn on processing again...
        this.processingBlock = true;

        // Signal that mining can begin...
        //System.out.println("criteria.countDown()");
        this.criteria.countDown();
    }

    @Override
    // Performs communications loop...
    public void run() {
        //System.out.println("Communicator: rank=" + rank);

        try {
            while(processingBlocks) {
                // Wait for criteria to be posted to us...
                //System.out.println("criteria.await()");
                this.criteria.await();
                //System.out.println("past criteria.await()");

                // Reset our criteria latch for next round...
                this.criteria = new CountDownLatch(1);

                while (processingBlock) {

                    // Wait for a response...
                    NonceManagerMessage msg = null;
                    try {
                        //msg = Utilities.receiveMessage(socket);
                        msg = (edu.rit.cs.MNMT.NonceManagerMessage) objectInputStream.readObject();
                    } catch (StreamCorruptedException | SocketException e) {
                        System.out.println("NonceManagerCommunicator detected corrupted stream, closing socket.");
                        try {
                            socket.close();
                        } catch (IOException ex) {
                            return;
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        //System.out.println("IOException or ClassNotFoundException");
                        try {
                            socket.close();
                        } catch (IOException ex) {
                            return;
                        }
                    }

                    if (msg != null) {
                        switch (msg.type) {
                            case NonceManagerMessage.MSG_TYPE_REQUEST:
                                //System.out.println("Need to send another nonce range to slave...");
                                //System.out.println("HashRatePerMillis=" + msg.hashRatePerMillis);

                                // Record the currentTime in millis (this is the "Start" of the nonce assignment
                                long startAssignmentTime = System.currentTimeMillis();

                                // Initialize the NonceSlave with a NonceManagerMessage
                                NonceManagerMessage initialMessage = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_INITIALIZE, 0, rank, 0, 0, blockHash, targetHash, threads, startAssignmentTime);

                                // Update the NonceManagerMessage with the assigned nonce range
                                boolean success = manager.assignNextNonceRange(initialMessage, msg.hashRatePerMillis);

                                // Send the message to the slave...
                                this.sendMessage(initialMessage);
                                //Utilities.sendMessage(socket, initialMessage);

                                break;
                            case NonceManagerMessage.MSG_TYPE_WINNER:
                                if (blockHash.compareTo(msg.blockHash) == 0) {
                                    // Correcy block hash!
                                    if (targetHash.compareTo(msg.winningHash) > 0) {
                                        // Valid winner!

                                        // Notify listener about our winning nonce!
                                        System.out.println("\nWinning nonce found = " + msg.winningNonce + ", hash=" + msg.winningHash);
                                        processingBlock = false;
                                        manager.setWinner(msg.winningNonce, msg.winningHash);
                                    } else {
                                        //System.out.println("\n(Ignoring) Incorrect winning nonce found = " + msg.winningNonce + ", hash=" + msg.winningHash);
                                    }
                                } else {
                                    //System.out.println("\n(Ignoring) winning nonce for incorrect block hash = " + msg.winningNonce + ", hash=" + msg.winningHash);
                                }
                                break;

                            case NonceManagerMessage.MSG_TYPE_CHECKOUT_REQUEST:
                                //System.out.println("Received checkout request from host=" + msg.checkoutHostname + " for deviceId=" + msg.checkoutDeviceId);

                                // Prepare a msg template for the response
                                NonceManagerMessage responseMsg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_CHECKOUT_RESPONSE, 0, msg.source, msg.checkoutHostname,
                                        msg.checkoutDeviceId, false);

                                // Check to see if the device is available
                                if(manager.checkoutDevice(msg.checkoutHostname, msg.checkoutDeviceId)) {
                                    // Send success!
                                    responseMsg.checkoutResponse = true;
                                    sendMessage(responseMsg);
                                } else {
                                    // Send failure
                                    sendMessage(responseMsg);
                                }
                                break;
                        }
                    } else {
                        // Failed to receive message, probably Slave socket broken, stop processing
                        //System.out.println("Failed to receive message?");
                        processingBlock = false;
                    }

                }
            }
        } catch (/*IOException |*/ InterruptedException e) {
            System.out.println("ERROR: IOException or InterruptedException in NonceManagerCommunicator.run()");
            e.printStackTrace();
        }
    }

    // Helper method to tell if still processing a block
    public boolean isProcessingBlock() {
        return processingBlock;
    }

    // Helper method to stop processing the current block
    public void stopProcecssingThisBlock() {
        // Notify Slave to STOP!
        NonceManagerMessage msg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_STOP, 0, 0);
        this.sendMessage(msg);

        // Disable block and blocks processing...
        processingBlock = false;
    }

    // Helper method to stop processing
    public void stopProcessing() {
        // Notify Slave to STOP!
        NonceManagerMessage msg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_STOP, 0, 0);
        //Utilities.sendMessage(socket, msg);
        this.sendMessage(msg);

        // Disable block and blocks processing...
        processingBlock = false;

        // Unlock our latch if it is locked...
        criteria.countDown();
    }

    // Helper method to terminate processing
    public void terminateProcessing() {
        // Notify Slave to TERMINATE!
        NonceManagerMessage msg = new NonceManagerMessage(NonceManagerMessage.MSG_TYPE_TERMINATE, 0, 0);
        this.sendMessage(msg);

        // Disable block and blocks processing...
        processingBlock = false;
        processingBlocks = false;

        // Unlock our latch if it is locked...
        criteria.countDown();
    }

    // Helper method to send a message to a Slave
    public void sendMessage(NonceManagerMessage msg) {
        //System.out.print("Sending " + Utilities.msgType(msg) + " msg.");
        this.sendQueue.addMsg(msg);
    }
}
