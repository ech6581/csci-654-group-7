/**
 * Code originally from https://github.com/moffa13/SHA256CUDA/tree/master/SHA256CUDA
 *
 * Adapted for CSCI-654 Group Project
 */

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <iostream>
#include <chrono>
#include <cmath>
#include <thread>
#include <iomanip>
#include <string>
#include <cassert>
//#include "main.h"

// Beginning of SHA256.cuh
#ifndef SHA256_H
#define SHA256_H

#include <cstdint>


/****************************** MACROS ******************************/
#define SHA256_BLOCK_SIZE 32            // SHA256 outputs a 32 byte digest

#define ROTLEFT(a,b) (((a) << (b)) | ((a) >> (32-(b))))
#define ROTRIGHT(a,b) (((a) >> (b)) | ((a) << (32-(b))))

#define CH(x,y,z) (((x) & (y)) ^ (~(x) & (z)))
#define MAJ(x,y,z) (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)))
#define EP0(x) (ROTRIGHT(x,2) ^ ROTRIGHT(x,13) ^ ROTRIGHT(x,22))
#define EP1(x) (ROTRIGHT(x,6) ^ ROTRIGHT(x,11) ^ ROTRIGHT(x,25))
#define SIG0(x) (ROTRIGHT(x,7) ^ ROTRIGHT(x,18) ^ ((x) >> 3))
#define SIG1(x) (ROTRIGHT(x,17) ^ ROTRIGHT(x,19) ^ ((x) >> 10))

#define checkCudaErrors(x) \
{ \
    cudaGetLastError(); \
    x; \
    cudaError_t err = cudaGetLastError(); \
    if (err != cudaSuccess) \
        printf("GPU: cudaError %d (%s)\n", err, cudaGetErrorString(err)); \
}
/**************************** DATA TYPES ****************************/
typedef unsigned char BYTE;             // 8-bit byte
typedef uint32_t  WORD;             // 32-bit word, change to "long" for 16-bit machines

typedef struct {
	BYTE data[64];
	WORD datalen;
	unsigned long long bitlen;
	WORD state[8];
} SHA256_CTX;

__constant__ WORD dev_k[64];

/*********************** FUNCTION DECLARATIONS **********************/
char * print_sha(BYTE * buff);
__device__ void sha256_init(SHA256_CTX *ctx);
__device__ void sha256_update(SHA256_CTX *ctx, const BYTE data[], size_t len);
__device__ void sha256_final(SHA256_CTX *ctx, BYTE hash[]);

__device__ void mycpy12(uint32_t *d, const uint32_t *s) {
#pragma unroll 3
	for (int k = 0; k < 3; k++) d[k] = s[k];
}

__device__ void mycpy16(uint32_t *d, const uint32_t *s) {
#pragma unroll 4
	for (int k = 0; k < 4; k++) d[k] = s[k];
}

__device__ void mycpy32(uint32_t *d, const uint32_t *s) {
#pragma unroll 8
	for (int k = 0; k < 8; k++) d[k] = s[k];
}

__device__ void mycpy44(uint32_t *d, const uint32_t *s) {
#pragma unroll 11
	for (int k = 0; k < 11; k++) d[k] = s[k];
}

__device__ void mycpy48(uint32_t *d, const uint32_t *s) {
#pragma unroll 12
	for (int k = 0; k < 12; k++) d[k] = s[k];
}

__device__ void mycpy64(uint32_t *d, const uint32_t *s) {
#pragma unroll 16
	for (int k = 0; k < 16; k++) d[k] = s[k];
}

__device__ void sha256_transform(SHA256_CTX *ctx, const BYTE data[])
{
	WORD a, b, c, d, e, f, g, h, i, j, t1, t2, m[64];
	//WORD S[8];

	//mycpy32(S, ctx->state);

#pragma unroll 16
	for (i = 0, j = 0; i < 16; ++i, j += 4)
		m[i] = (data[j] << 24) | (data[j + 1] << 16) | (data[j + 2] << 8) | (data[j + 3]);

#pragma unroll 64
	for (; i < 64; ++i)
		m[i] = SIG1(m[i - 2]) + m[i - 7] + SIG0(m[i - 15]) + m[i - 16];

	a = ctx->state[0];
	b = ctx->state[1];
	c = ctx->state[2];
	d = ctx->state[3];
	e = ctx->state[4];
	f = ctx->state[5];
	g = ctx->state[6];
	h = ctx->state[7];

#pragma unroll 64
	for (i = 0; i < 64; ++i) {
		t1 = h + EP1(e) + CH(e, f, g) + dev_k[i] + m[i];
		t2 = EP0(a) + MAJ(a, b, c);
		h = g;
		g = f;
		f = e;
		e = d + t1;
		d = c;
		c = b;
		b = a;
		a = t1 + t2;
	}

	ctx->state[0] += a;
	ctx->state[1] += b;
	ctx->state[2] += c;
	ctx->state[3] += d;
	ctx->state[4] += e;
	ctx->state[5] += f;
	ctx->state[6] += g;
	ctx->state[7] += h;
}

__device__ void sha256_init(SHA256_CTX *ctx)
{
	ctx->datalen = 0;
	ctx->bitlen = 0;
	ctx->state[0] = 0x6a09e667;
	ctx->state[1] = 0xbb67ae85;
	ctx->state[2] = 0x3c6ef372;
	ctx->state[3] = 0xa54ff53a;
	ctx->state[4] = 0x510e527f;
	ctx->state[5] = 0x9b05688c;
	ctx->state[6] = 0x1f83d9ab;
	ctx->state[7] = 0x5be0cd19;
}

__device__ void sha256_update(SHA256_CTX *ctx, const BYTE data[], size_t len)
{
	WORD i;

	// for each byte in message
	for (i = 0; i < len; ++i) {
		// ctx->data == message 512 bit chunk
		ctx->data[ctx->datalen] = data[i];
		ctx->datalen++;
		if (ctx->datalen == 64) {
			sha256_transform(ctx, ctx->data);
			ctx->bitlen += 512;
			ctx->datalen = 0;
		}
	}
}

__device__ void sha256_final(SHA256_CTX *ctx, BYTE hash[])
{
	WORD i;

	i = ctx->datalen;

	// Pad whatever data is left in the buffer.
	if (ctx->datalen < 56) {
		ctx->data[i++] = 0x80;
		while (i < 56)
			ctx->data[i++] = 0x00;
	}
	else {
		ctx->data[i++] = 0x80;
		while (i < 64)
			ctx->data[i++] = 0x00;
		sha256_transform(ctx, ctx->data);
		memset(ctx->data, 0, 56);
	}

	// Append to the padding the total message's length in bits and transform.
	ctx->bitlen += ctx->datalen * 8;
	ctx->data[63] = ctx->bitlen;
	ctx->data[62] = ctx->bitlen >> 8;
	ctx->data[61] = ctx->bitlen >> 16;
	ctx->data[60] = ctx->bitlen >> 24;
	ctx->data[59] = ctx->bitlen >> 32;
	ctx->data[58] = ctx->bitlen >> 40;
	ctx->data[57] = ctx->bitlen >> 48;
	ctx->data[56] = ctx->bitlen >> 56;
	sha256_transform(ctx, ctx->data);

	// Since this implementation uses little endian byte ordering and SHA uses big endian,
	// reverse all the bytes when copying the final state to the output hash.
	for (i = 0; i < 4; ++i) {
		hash[i] = (ctx->state[0] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 4] = (ctx->state[1] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 8] = (ctx->state[2] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 12] = (ctx->state[3] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 16] = (ctx->state[4] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 20] = (ctx->state[5] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 24] = (ctx->state[6] >> (24 - i * 8)) & 0x000000ff;
		hash[i + 28] = (ctx->state[7] >> (24 - i * 8)) & 0x000000ff;
	}
}

#endif   // SHA256_H
// End of SHA256.cuh

//#include "sha256.cuh"

//static size_t difficulty = 1;

// Output string by the device read by host
char *g_out = nullptr;
unsigned char *g_hash_out = nullptr;
int *g_found = nullptr;

__device__ bool checkZeroPadding(unsigned char* sha, size_t difficulty) {

	for (size_t cur_byte = 0; cur_byte < difficulty / 2; ++cur_byte) {
		if (sha[cur_byte] != 0) {
			return false;
		}
	}

	bool isOdd = difficulty % 2 != 0;
	size_t last_byte_check = static_cast<size_t>(difficulty / 2);
	if (isOdd) {
		if (sha[last_byte_check] > 0x0F || sha[last_byte_check] == 0) {
			return false;
		}
	}
	else if (sha[last_byte_check] < 0x0F) return false;

	return true;
}

// Does the same as sprintf(char*, "%d%s", int, const char*) but a bit faster
__device__ size_t nonce_to_str(uint64_t nonce, unsigned char* out) {
	uint64_t result = nonce;
	uint8_t remainder;
	size_t nonce_size = nonce == 0 ? 1 : floor(log10((double)nonce)) + 1;
	size_t i = nonce_size;
	while (result >= 10) {
		remainder = result % 10;
		result /= 10;
		out[--i] = remainder + '0';
	}

	out[0] = result + '0';
	i = nonce_size;
	out[i] = 0;
	return i;
}


/*// Does the same as sprintf(char*, "%d%s", int, const char*) but a bit faster
__device__ size_t nonce_to_str2(uint64_t nonce, char* out) {

    sprintf(out, "%d", nonce);
    return strlen(out);
}*/

/* A utility function to reverse a string  */
// Code taken from: https://www.geeksforgeeks.org/implement-itoa/
__device__ void reverse(char *str, int length)
{
    int start = 0;
    int end = length -1;
    while (start < end)
    {
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;
        //swap((str+start), (str+end));
        start++;
        end--;
    }
}

// Implementation of itoa()
// Code taken from: https://www.geeksforgeeks.org/implement-itoa/
__device__ int itoa(int num, char* str, int base)
{
    int i = 0;
    bool isNegative = false;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return i;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    reverse(str, i);

    return i;
}

// Utility function to count the length of the given string
__device__ size_t stringSize(char* string) {
    int i = 0;

    while(string[i] != 0) {
        i++;
    }

    return i;
}

// Utility function to concatenate two strings into a buffer, first then second.
__device__ char* stringConcatenate(char* buffer, char* first, char* second) {
    int i = 0;
    int firstLength = stringSize(first);
    for(i = 0; i < firstLength; i++) {
        buffer[i] = first[i];
    }

    int secondLength = stringSize(second);
    for(; i < (firstLength+secondLength); i++) {
        buffer[i] = second[i - firstLength];
    }

    return buffer;
}

// Utility method to compare sha and target hash
__device__ int checkTarget(unsigned char* sha, unsigned char* target) {
    for(int i = 0; i < 32; i++) {
        if(sha[i] < target[i]) {
            // sha is better than target
            return 1;
        } else if(sha[i] == target[i]) {
            // sha is same as target
            continue;
        }

        // sha is worse than target
        return 0;
    }

    return 0;
}

__constant__ char dev_HEXBYTES[16];

// Utility function to convert a bytes array into a hex string array
// Based upon: http://www.java2s.com/Code/Java/File-Input-Output/Convertsabytearrayintoahexadecimalstring.htm
__device__ void bytesToHex(char* b, char* s) {
    int i = 0;
    int j = 0;
    for (;  i < 32; i++) {
        int c = ((int) b[i]) & 0xff;

        s[j++] = (char) dev_HEXBYTES[c >> 4 & 0xf];
        s[j++] = (char) dev_HEXBYTES[c & 0xf];
    }
}

extern __shared__ char array[];

// Alternate kernel
extern "C"
__global__ void sha256_kernel_vertical(int* out_nonce, char* out_input_string_nonce, unsigned char* out_found_hash, int *out_found, unsigned char* targetHashBytes, char* in_input_string, int in_input_string_size, int nonce_range, long nonce_offset) {
	// Locally allocate our intermediateCtx
    SHA256_CTX intermediateCtx;

	// Perform the initialization of our intermediate CTX by pre-hashing our input string
    sha256_init(&intermediateCtx);                                              // Initialize context
    sha256_update(&intermediateCtx, (unsigned char *)in_input_string, in_input_string_size); // Hash in the rest of the input

    // Setup our working memory...
    char shaString[32];
    char shaStringHex[64];
    char nonceString[32];
    unsigned char doubleShaString[32];

    // Perform SHA256 - Round #1
    SHA256_CTX ctx;
    SHA256_CTX ctx2;

    memset(nonceString, 0, 32);
    memset(shaString, 0, 32);
    memset(shaStringHex, 0, 64);
    memset(doubleShaString, 0, 32);

    // Initialize our nonce
    long nonce = blockIdx.x * blockDim.x + threadIdx.x + nonce_offset;

    int count = 0;

    int limit = nonce_range / blockDim.x;

    // Search using idx stride access to iterate through the nonce range...
    while(out_found[0] == 0 && count < limit) {

        // Convert nonce into a string...
        int size = itoa(nonce, (char *) nonceString, 10);

        // Copy intermediate SHA256_CTX into our local version, this contains the already hashed input string!
        memcpy(&ctx, &intermediateCtx, sizeof(SHA256_CTX));
        sha256_update(&ctx, (unsigned char*) nonceString, size);                // Hash in the nonce
        sha256_final(&ctx, (unsigned char *)shaString);                              // Generate final SHA256 hash in BYTES (not in HEX)

        // Convert shaString bytes to hex
        bytesToHex(shaString, shaStringHex);

        // Perform SHA256 - Round #2
        sha256_init(&ctx2);
        sha256_update(&ctx2, (unsigned char*) shaStringHex, 64);
        sha256_final(&ctx2, doubleShaString);

        if(checkTarget((unsigned char*) doubleShaString, targetHashBytes) && atomicExch(out_found, 1) == 0) {
            memcpy(out_found_hash, doubleShaString, 32);                            // SHA256 hash
            memcpy(out_input_string_nonce, nonceString, size);                  // winning nonce as a String

            // Return the nonce
            out_nonce[0] = nonce;
            printf("count=%d, nonce_range=%d, limit=%d\n", count, nonce_range, limit);
            break;
        }

        // Increment nonce...
        //nonce += blockIdx.x * blockDim.x;
        //nonce += 1024;
        nonce += blockDim.x;

        count++;
    }
}

// Final kernel
extern "C"
__global__ void sha256_kernel(int* out_nonce, char* out_input_string_nonce, unsigned char* out_found_hash, int *out_found, unsigned char* targetHashBytes, char* in_input_string, int in_input_string_size, int difficulty, long nonce_offset) {

	// If this is the first thread of the block, init the intermediate SHA256_CTX into shared memory
    SHA256_CTX* intermediateCtx = (SHA256_CTX*) &array[0];

	if (threadIdx.x == 0) {
		// Perform the initialization of our intermediate CTX by pre-hashing our input string
        sha256_init(intermediateCtx);                                              // Initialize context
        sha256_update(intermediateCtx, (unsigned char *)in_input_string, in_input_string_size); // Hash in the rest of the input
	}

    // Barrier for all threads so that we can cache values in shared memory for faster access
	__syncthreads(); // Ensure the input string has been SHA256'd

	uint64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    long nonce = idx + nonce_offset;

    // Setup our working memory...
    char shaString[32];
    char shaStringHex[64];
	char nonceString[32];
    unsigned char doubleShaString[32];
	memset(nonceString, 0, 32);
	memset(shaString, 0, 32);
	memset(shaStringHex, 0, 64);
	memset(doubleShaString, 0, 32);

    //memset(intermediateString, 0, 32);
	int size = itoa(nonce, (char *) nonceString, 10);

    // Perform SHA256 - Round #1
	SHA256_CTX ctx;
	// Copy intermediate SHA256_CTX into our local version, this contains the already hashed input string!
	memcpy(&ctx, intermediateCtx, sizeof(SHA256_CTX));
	sha256_update(&ctx, (unsigned char*) nonceString, size);   // Hash in the nonce
	sha256_final(&ctx, (unsigned char *)shaString);            // Generate Round #1 SHA256 hash in binary (not in HEX)

    // Convert shaString bytes to hex
    bytesToHex(shaString, shaStringHex);

    // Perform SHA256 - Round #2
    SHA256_CTX ctx2;
    sha256_init(&ctx2);
    sha256_update(&ctx2, (unsigned char*) shaStringHex, 64);
    sha256_final(&ctx2, doubleShaString);                      // Generate Round #2 SHA256 hash in binary

    if(checkTarget((unsigned char*) doubleShaString, targetHashBytes) && atomicExch(out_found, 1) == 0) {
        memcpy(out_found_hash, doubleShaString, 32);                        // SHA256 hash
        memcpy(out_input_string_nonce, nonceString, size);                  // winning nonce as a String

        // Return the nonce
        out_nonce[0] = nonce;
    }
}

// Earlier kernel
extern "C"
__global__ void sha256_kernelBEFOREBinaryCompare(int* out_nonce, char* out_input_string_nonce, unsigned char* out_found_hash, int *out_found, unsigned char* targetHashBytes, char* in_input_string, int in_input_string_size, int difficulty, long nonce_offset) {

	// If this is the first thread of the block, init the intermediate SHA256_CTX into shared memory
    SHA256_CTX* intermediateCtx = (SHA256_CTX*) &array[0];

	if (threadIdx.x == 0) {
		// Perform the initialization of our intermediate CTX by pre-hashing our input string
        sha256_init(intermediateCtx);                                              // Initialize context
        sha256_update(intermediateCtx, (unsigned char *)in_input_string, in_input_string_size); // Hash in the rest of the input

        // Afterwards, copies of this intermediate SHA256_CTX can be used as our starting point for hashing in the rest of the data!
	}

    // Barrier for all threads so that we can cache values in shared memory for faster access
	__syncthreads(); // Ensure the input string has been written in SMEM

    // The first byte we can write because there is the input string at the begining
	// Respects the memory padding of 8 bit (char).
	//size_t const minArray = static_cast<size_t>(ceil((in_input_string_size + 152 + 1) / 8.f) * 8);
	//uintptr_t intermediate_addr = threadIdx.x * (64) + minArray;
	//unsigned char* intermediateString = (unsigned char*)&array[intermediate_addr];

	uint64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    long nonce = idx + nonce_offset;

    // Setup our working memory...
    char shaString[32];
    char shaStringHex[64];
	char nonceString[32];
    unsigned char doubleShaString[32];
	memset(nonceString, 0, 32);
	memset(shaString, 0, 32);
	memset(shaStringHex, 0, 64);
	memset(doubleShaString, 0, 32);

    //memset(intermediateString, 0, 32);
	int size = itoa(nonce, (char *) nonceString, 10);

    // Perform SHA256 - Round #1
	SHA256_CTX ctx;
	// Copy intermediate SHA256_CTX into our local version, this contains the already hashed input string!
	memcpy(&ctx, intermediateCtx, sizeof(SHA256_CTX));
	sha256_update(&ctx, (unsigned char*) nonceString, size);                // Hash in the nonce
	sha256_final(&ctx, (unsigned char *)shaString);                              // Generate final SHA256 hash in BYTES (not in HEX)

    // Convert shaString bytes to hex
    bytesToHex(shaString, shaStringHex);

    // Perform SHA256 - Round #2
    SHA256_CTX ctx2;
    sha256_init(&ctx2);
    sha256_update(&ctx2, (unsigned char*) shaStringHex, 64);
    sha256_final(&ctx2, doubleShaString);

    if(checkTarget((unsigned char*) doubleShaString, targetHashBytes) && atomicExch(out_found, 1) == 0) {
        //printf("sha: %d %d %d %d %d %d\n", sha[0], sha[1], sha[2], sha[3], sha[4], sha[5]);
        //printf("tar: %d %d %d %d %d %d\n", targetHashBytes[0], targetHashBytes[1], targetHashBytes[2], targetHashBytes[3], targetHashBytes[4], targetHashBytes[5]);
        //printf("shaString bytes: %d %d %d %d %d\n", (unsigned char) shaString[0], (unsigned char)shaString[1], (unsigned char)shaString[2], (unsigned char)shaString[3], (unsigned char)shaString[4]);
        //printf("shaStringHex   : %c %c %c %c %c\n", shaStringHex[0], shaStringHex[1], shaStringHex[2], shaStringHex[3], shaStringHex[4]);
        memcpy(out_found_hash, doubleShaString, 32);                            // SHA256 hash
        memcpy(out_input_string_nonce, nonceString, size);                  // winning nonce as a String
        //memcpy(out_input_string_nonce + size, in, in_input_string_size + 1);    // input string

        // Return the nonce
        out_nonce[0] = nonce;
    }
}

// Earlier kernel
extern "C"
__global__ void sha256_kernelNEWSLOW(int* out_nonce, char* out_input_string_nonce, unsigned char* out_found_hash, int *out_found, unsigned char* targetHashBytes, char* in_input_string, int in_input_string_size, int difficulty, long nonce_offset) {

	// If this is the first thread of the block, init the input string in shared memory
	char* in = (char*) &array[0];
	if (threadIdx.x == 0) {
		memcpy(in, in_input_string, in_input_string_size + 1);
	}


    // Barrier for all threads so that we can cache values in shared memory for faster access
	__syncthreads(); // Ensure the input string has been written in SMEM

    // The first byte we can write because there is the input string at the begining
	// Respects the memory padding of 8 bit (char).
	size_t const minArray = static_cast<size_t>(ceil((in_input_string_size + 152 + 1) / 8.f) * 8);

	uintptr_t intermediate_addr = threadIdx.x * (64) + minArray;
	unsigned char* intermediateString = (unsigned char*)&array[intermediate_addr];

	uint64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    long nonce = idx + nonce_offset;

    // Setup our working memory...
    //char shaString[32];
    char shaStringHex[64];
	//char nonceString[32];
    //unsigned char doubleShaString[32];
	//memset(nonceString, 0, 32);
	//memset(shaString, 0, 32);
	memset(shaStringHex, 0, 64);
	//memset(doubleShaString, 0, 32);

    // Erase intermediate shared memory...
    memset(intermediateString, 0, 64);

    // Populate intermediate shared memory with nonceString
	itoa(nonce, (char *) intermediateString, 10);
	int size = stringSize((char *) intermediateString);

    // Perform SHA256 - Round #1
	SHA256_CTX ctx;
	sha256_init(&ctx);                                              // Initialize context
	sha256_update(&ctx, (unsigned char *)in, in_input_string_size); // Hash in the rest of the input
	sha256_update(&ctx, (unsigned char*) intermediateString, size);                // Hash in the nonce

	// Erase intermediate shared memory...
    memset(intermediateString, 0, 64);

	sha256_final(&ctx, (unsigned char *)intermediateString);                              // Generate final SHA256 hash in BYTES (not in HEX)

    // Convert shaString bytes to hex
    bytesToHex((char *) intermediateString, shaStringHex);

    // Perform SHA256 - Round #2
    SHA256_CTX ctx2;
    sha256_init(&ctx2);
    sha256_update(&ctx2, (unsigned char*) shaStringHex, 64);

    // Erase intermediate shared memory...
    memset(intermediateString, 0, 64);
    sha256_final(&ctx2, intermediateString);

    if(checkTarget((unsigned char*) intermediateString, targetHashBytes) && atomicExch(out_found, 1) == 0) {
        //printf("sha: %d %d %d %d %d %d\n", sha[0], sha[1], sha[2], sha[3], sha[4], sha[5]);
        //printf("tar: %d %d %d %d %d %d\n", targetHashBytes[0], targetHashBytes[1], targetHashBytes[2], targetHashBytes[3], targetHashBytes[4], targetHashBytes[5]);
        //printf("shaString bytes: %d %d %d %d %d\n", (unsigned char) shaString[0], (unsigned char)shaString[1], (unsigned char)shaString[2], (unsigned char)shaString[3], (unsigned char)shaString[4]);
        //printf("shaStringHex   : %c %c %c %c %c\n", shaStringHex[0], shaStringHex[1], shaStringHex[2], shaStringHex[3], shaStringHex[4]);
        memcpy(out_found_hash, intermediateString, 32);                            // SHA256 hash
        memcpy(out_input_string_nonce, intermediateString, size);                  // winning nonce as a String
        //memcpy(out_input_string_nonce + size, in, in_input_string_size + 1);    // input string

        // Return the nonce
        out_nonce[0] = nonce;
    }
}

// Earlier kernel
extern "C"
__global__ void sha256_kernelOLD(int* out_nonce, char* out_input_string_nonce, unsigned char* out_found_hash, int *out_found, unsigned char* targetHashBytes, char* in_input_string, int in_input_string_size, int difficulty, long nonce_offset) {

	uint64_t idx = blockIdx.x * blockDim.x + threadIdx.x;
    long nonce = idx + nonce_offset;

    // Setup our working memory...
    char shaString[32];
    char shaStringHex[64];
	char nonceString[32];
    unsigned char doubleShaString[32];
	memset(nonceString, 0, 32);
	memset(shaString, 0, 32);
	memset(shaStringHex, 0, 64);
	memset(doubleShaString, 0, 32);

	itoa(nonce, nonceString, 10);
	int size = stringSize(nonceString);

    // Perform SHA256 - Round #1
	SHA256_CTX ctx;
	sha256_init(&ctx);                                              // Initialize context
	sha256_update(&ctx, (unsigned char *)in_input_string, in_input_string_size); // Hash in the rest of the input
	sha256_update(&ctx, (unsigned char*) nonceString, size);                // Hash in the nonce
	sha256_final(&ctx, (unsigned char *)shaString);                              // Generate final SHA256 hash in BYTES (not in HEX)

    // Convert shaString bytes to hex
    bytesToHex(shaString, shaStringHex);

    // Perform SHA256 - Round #2
    SHA256_CTX ctx2;
    sha256_init(&ctx2);
    sha256_update(&ctx2, (unsigned char*) shaStringHex, 64);
    sha256_final(&ctx2, doubleShaString);

    if(checkTarget((unsigned char*) doubleShaString, targetHashBytes) && atomicExch(out_found, 1) == 0) {
        //printf("sha: %d %d %d %d %d %d\n", sha[0], sha[1], sha[2], sha[3], sha[4], sha[5]);
        //printf("tar: %d %d %d %d %d %d\n", targetHashBytes[0], targetHashBytes[1], targetHashBytes[2], targetHashBytes[3], targetHashBytes[4], targetHashBytes[5]);
        //printf("shaString bytes: %d %d %d %d %d\n", (unsigned char) shaString[0], (unsigned char)shaString[1], (unsigned char)shaString[2], (unsigned char)shaString[3], (unsigned char)shaString[4]);
        //printf("shaStringHex   : %c %c %c %c %c\n", shaStringHex[0], shaStringHex[1], shaStringHex[2], shaStringHex[3], shaStringHex[4]);
        memcpy(out_found_hash, doubleShaString, 32);                            // SHA256 hash
        memcpy(out_input_string_nonce, nonceString, size);                  // winning nonce as a String
        //memcpy(out_input_string_nonce + size, in, in_input_string_size + 1);    // input string

        // Return the nonce
        out_nonce[0] = nonce;
    }
}