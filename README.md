CSCI-654 Group Project for Group 7

The Java source code for solution can be found in ./edu/rit/cs/MNMT.

The CUDA kernel source code for solution can be found in ./resources.

The dependent libraries for JCUDA and Apache Commons can be found in ./lib.

#CoinMining_MNMT_MPI
To run using mpi, run the following on the Master node.

```ssh tardis.cs.rit.edu```

```time mpirun --hostfile gridSearchNodesGPU.txt --prefix /usr/local java -cp csci-654-group-7.jar edu.rit.cs.MNMT.CoinMining_MNMT_MPI tardis.cs.rit.edu 5554 10 300000 400000000 8000000```

(optional) Replace hostfile with alternate host files, and launch from the first node in file.

#CoinMining_MNMT (non-MPI)

To run manually, without mpirun, run the following on EACH Slave node:

```time java -cp csci-654-group-7.jar edu.rit.cs.MNMT.CoinMining_MNMT slave tardis.cs.rit.edu 5554 400000000 4500000```

To run manually, without mpirun, run the following on the Master node:

```time java -cp csci-654-group-7.jar edu.rit.cs.MNMT.CoinMining_MNMT master 5554 10 300000```

